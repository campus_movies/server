package fr.campus.campusmovie.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "movies")
public class Movie {
    
    @Id
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "poster_path")
    private String poster_path;

    @Column(name = "title", length = 255, nullable = false)
    private String title;

    @Column(name = "overview", length = 1000)
    private String overview;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPoster_path() {
        return this.poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return this.overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }
}
