package fr.campus.campusmovie.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.campus.campusmovie.models.Movie;
import fr.campus.campusmovie.repository.MovieRepository;

@Service
public class MovieService {

    @Autowired
    MovieRepository movieRepository;
    
    public List<Movie> getAllMovies() {
        return this.movieRepository.findAll();
    }

    @Transactional
    public void saveMultipleMovie(List<Movie> movies) {
        this.movieRepository.saveAll(movies);
    }
}
