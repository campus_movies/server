package fr.campus.campusmovie.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.campus.campusmovie.models.Movie;

@Repository
public interface MovieRepository extends CrudRepository<Movie, Integer> {
    
    public List<Movie> findAll();
}
