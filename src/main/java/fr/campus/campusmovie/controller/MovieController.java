package fr.campus.campusmovie.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.campus.campusmovie.models.Movie;
import fr.campus.campusmovie.service.MovieService;

@RestController
@RequestMapping("/movie")
public class MovieController {
    
    @Autowired
    MovieService movieService;

    @GetMapping("/all")
    public List<Movie> getAllMovies() {
        return this.movieService.getAllMovies();
    }

    @PostMapping("/add")
    @ResponseStatus(code = HttpStatus.CREATED)
    public void addMultipleMovies(@RequestBody List<Movie> movies) {
        this.movieService.saveMultipleMovie(movies);
    }
}
