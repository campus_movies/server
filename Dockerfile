#
# BUILD
#
FROM maven:3.8.4-openjdk-11-slim AS campusmovie_build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean package

#
# RUN
#
FROM openjdk:11-jre-slim
COPY --from=campusmovie_build /usr/src/app/target/campusmovie-0.0.1-SNAPSHOT.jar /usr/app/campusmovie-0.0.1-SNAPSHOT.jar
EXPOSE 3001
ENTRYPOINT ["java","-jar","/usr/app/campusmovie-0.0.1-SNAPSHOT.jar"] 