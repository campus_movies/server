# server

## Démarer avec docker

### Pré-requis
* docker
* docker-compose

### Commandes
Il faut être à la racine du projet pour lancer les commandes
* Lancer le server : `docker-compose up -d` <br>
  Le projet s'execute sur le port ***3001***

* Arrêter : `docker-compose down`
* Arrêter et supprimer l'image at les volumes associé : `docker-compose down -v --rmi all`

---

## Démarer sans docker

## Pré-requis
* JDK 11
* Maven

## Installation
Il faut être à la racine du projet pour lancer les commandes

* Génération des ressources : `./mvnw generate-resources`
* Installation des dépandances : `mvn install`

## Execution
Pour executer le projet : `mvn spring-boot:run`

Le projet s'execute sur le port ***3001***

## Test
à faire